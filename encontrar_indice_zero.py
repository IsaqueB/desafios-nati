def encontrar_indice_zero(cadeia):
    zero_anterior = 0
    tamanho_antes_zero_atual = 0
    indice_zero_maior_tamanho = 0
    maior_tamanho = 0
    tamanho = 0
    for x in range(len(cadeia)):
        if cadeia[x] == "0":
            if tamanho_antes_zero_atual+tamanho+1 > maior_tamanho:
                maior_tamanho = tamanho_antes_zero_atual+tamanho+1
                indice_zero_maior_tamanho = zero_anterior
            tamanho_antes_zero_atual = tamanho
            tamanho = 0
            zero_anterior = x
        else:
            tamanho += 1
    return indice_zero_maior_tamanho+1
print(encontrar_indice_zero("01001011100111111110111111010111"))
