__cargos = []
__funcionarios = []
def CadastrarCargo(salario):
    __cargos.append(salario)
def CadastrarFuncionario(codigo,nome,codigo_cargo):
    codigo_novo = True
    for funcionario in __funcionarios:
        if funcionario[0] == codigo:
            codigo_novo = False
            break
    if codigo_novo:
        if codigo_cargo < len(__cargos):
            __funcionarios.append([codigo,nome,codigo_cargo])
def MostrarRelatorio():
    relatorio = []
    for funcionario in __funcionarios:
        relatorio.append([funcionario[0],funcionario[1],__cargos[funcionario[2]]])
    return relatorio
def TotalSalarioPago(codigo_cargo):
    if codigo_cargo < len(__cargos):
        total_salario_pago = 0
        salario = __cargos[codigo_cargo]#Pra não ter que ficar acessando __cargos pra cada soma
        for funcionario in __funcionarios:
            if funcionario[2] == codigo_cargo:
                total_salario_pago += salario
        return total_salario_pago
